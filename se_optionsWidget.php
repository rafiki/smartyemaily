<?php
/**
 * se_optionsWidget
 */
class se_optionsWidget extends WP_Widget {
    /** constructor */
    function __construct() {
        parent::__construct(false, $name = 'SE Notifications Widget');        
    }

				static function init(){
						require_once('se_optionsWidgetAJAX.php');
						if(class_exists('se_optionsWidgetAJAX')){
							se_optionsWidgetAJAX::init();
						}
						self::enqueueCSS();
						self::enqueueJS();
				}
				
				static function enqueueCSS(){
					wp_register_style('seCSS', plugins_url('/css/smartyemaily.css', __FILE__));
					wp_enqueue_style('seCSS');
				}
				
				static function enqueueJS(){
	    	wp_register_script('se_optionsWidgetJS', plugins_url('js/se_optionsWidget.js', __FILE__), array( 'jquery' ));
	    	wp_enqueue_script( 'se_optionsWidgetJS' );
	    	wp_enqueue_script( 'jquery-form', array( 'jquery' ));
						wp_localize_script( 'se_optionsWidgetJS', 'seJS', array( 
								'ajaxurl' => admin_url( 'admin-ajax.php' ),
								'seNonce' => wp_create_nonce( 'se_nonce' ),
								'IMAGE_PATH' => plugins_url( 'images/', __FILE__)
						));
				}

    /** @see WP_Widget::widget */
   	function widget($args, $instance) {
					global $current_user;
					extract( $args );
    	$title = apply_filters('widget_title', $instance['se_options']['title']);
					
					//Load necessary variables
					$se_catSubscribers = get_option('se_catSubscribers');					
    	$notificationCats  = $instance['se_options']['notificationCats'];
					$commentNotification = $instance['se_options']['commentNotification'];
					
					//print_r($se_catSubscribers);
					
					//Render selectable categories
					if( !empty($notificationCats) ){
						$html .= '<p><b>Receive new post notifications in:</b></p>';
						$html .= '<select id="' . $widget_id . '-dropdown" name="' . $widget_id . '-dropdown">';
						foreach($notificationCats as $catID){
							$cat = get_category( $catID );
							$html .= '<option id="se_emailCat[' . $catID . ']" name="se_emailCat[' . $catID . ']" value="' . $catID . '">' . $cat->cat_name . '</option>';
						}
						$html .= '</select>';
						$html .= '<button type="button" id="addEmailCat" name="addEmailCat" class="se_addEmailCats" data-widgetid="' . $widget_id . '" data-userid="' . $current_user->ID . '">Add</button>';
					}
			
					//List of user subscriptions
					$html .= '<div id="' . $widget_id . '-subscriptions">';
						
						if( !empty($se_catSubscribers) ){
							$deleteImgSrc = plugins_url('images/no.png', __FILE__);
							foreach($se_catSubscribers as $catID => $subcribers){
								if( $subcribers[$current_user->ID] ){
									$cat = get_category($catID);
									$html .= '<p class="se_subscription">' . $cat->cat_name . '<img src="' . $deleteImgSrc . '" id="' . $widget_id . '-delete" data-catid="' . $catID . '" data-userid="' . $current_user->ID . '" class="se_removeSubscription" alt="Remove Subscription" title="Remove Subscription" /></p>';
								}
							}
						}
						
					$html .= '</div>';
					
					/*Render Comment Notification option
					if( !empty($commentNotification) ){
							$commentChecked = (bool) $userSettings['emailComments'] ? 'checked="checked' : '';				
							$html .= '<p><b>Comment Notifications</b></p>';
							$html .= '<input type="checkbox" id="se_emailComments" name="se_emailComments" ' . $commentChecked . ' />';
							$html .= '<label for="se_emailComments"> Receive notifications when someone comments on your post</label>';
					}*/
					$html .= '<div class="clear"></div>';
						
					//Render the widget
     echo $before_widget;
    	echo $before_title . $title . $after_title;
					echo '<div class="clear"></div>';
					echo '<form id="se_settingsForm" action="" method="post">' . $html . '</form>';
     echo $after_widget;
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {															
						$instance['se_options'] = $new_instance['se_options'];
      return $instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
					$id   = $this->get_field_id( 'se_options' );
					$name = $this->get_field_name( 'se_options' );
										
					if ( isset( $instance['se_options'][ 'title' ] ) ) {
						$title = $instance['se_options'][ 'title' ];
					} else {
						$title = __( 'New title', 'text_domain' );
					}
					
					$html .= '<p>';
						$html .= '<label for="'. $id . '[title]">' . _e( 'Title:' ) . '</label>';
					 $html .= '<input class="widefat" id="' . $id . '[title]" name="' . $name . '[title]" type="text" value="' . esc_attr( $title ) . '" />';
					$html .= '</p>';
					
					$args = array(
					'type'                     => 'post',
					'orderby'                  => 'name',
					'order'                    => 'DESC',
					'hide_empty'               => 0,
					'hierarchical'             => 1,
					'taxonomy'                 => 'category',
					'pad_counts'               => false );
					$allCats = get_categories($args);

					//If $instance is not already set, load all categories by default
					if ( !is_array( $instance['se_options']['notificationCats'] ) ){
							foreach($allCats as $cat){
								$defaultCats[$cat->term_id] = true;
							}
							$instance['se_options']['notificationCats'] = $defaultCats;
							$instance['se_options']['commentNotification'] = true;
					}
					unset($cat);
					
					$html   .= '<p><b> Select which categories users can subscribe to: </b></p>';
					foreach ($allCats as $cat) {
						$checked = in_array($cat->term_id, $instance['se_options']['notificationCats']) ? 'checked="checked"' : '';
						$html   .= '<input type="checkbox" id="' . $id . '[notificationCats][]" name="' . $name . '[notificationCats][]" value="' . $cat->term_id . '" ' . $checked . ' />';
						$html   .= '<label for="' . $id . '[notificationCats][]"> ' . $cat->cat_name . '</label>';						
      $html   .= '<br />';
	    }
	    
	    $commentsEnabled =  isset($instance['se_options']['commentNotification']) ? "1" : "0";
	    $commentsChecked =  (bool) $commentsEnabled ? 'checked="checked"' : '';

	    /*$html .= '<br />';
	    $html .= '<p><b> Enable comment subscriptions: </b></p>';
	    $html .= '<input type="checkbox" id="' . $id . '[commentNotification]" name="' . $name . '[commentNotification]" value="' . $commentsEnabled .'" ' . $commentsChecked . ' />';
					$html .= '<label for="' . $id . '[commentNotification]"> Enable option for users to subscribe to comment notifications on their own posts.</label>';
					$html .= '<br />';*/
					
	    echo $html;
    }

} 
?>