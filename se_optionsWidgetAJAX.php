<?php
/**
 * Handles JS AJAX requests coming from the front end.
 */
if(!class_exists('se_optionsWidgetAJAX')){
	class se_optionsWidgetAJAX{
		
		static public function init(){
		 add_action('wp_ajax_saveEmailCatAJAX', array('se_optionsWidgetAJAX', 'saveEmailCatAJAX'));
		 add_action('wp_ajax_deleteEmailCatAJAX', array('se_optionsWidgetAJAX', 'deleteEmailCatAJAX'));		 
		}
		
		/**
		 * Adds user subscriptions via AJAX
		 */
		function saveEmailCatAJAX(){
			$nonce = $_POST['nonce'];
			if( !wp_verify_nonce($nonce, 'se_nonce') ){
				header("HTTP/1.0 409 Security Check.");
				exit;
			}
			
			if( empty($_POST['userID']) ){
				header("HTTP/1.0 409 Could not find userID!");
				exit;
			}
			
			if( empty($_POST['catID']) ){
				header("HTTP/1.0 409 Could not find catID!");
				exit;				
			}
			
			$userID = (int) $_POST['userID'];
			global $current_user;
			
			if($userID != $current_user->ID){
				header("HTTP/1.0 409 User IDs do not match!");
				exit;
			}
			
			$catID  = (int) $_POST['catID'];
			
			$se_catSubscribers = get_option('se_catSubscribers');
			$user = get_userdata($userID);
			
			if( empty($se_catSubscribers) ){
				$se_catSubscribers[$catID][$userID] = $user->user_email;
			}else{
				if( !empty( $se_catSubscribers[$catID][$userID] ) ){
					header("HTTP/1.0 409 You are already subscribed to this category!");
					exit;
				}else{
					$se_catSubscribers[$catID][$userID] = $user->user_email;
				}
			}
			
			//update user meta with comment settings
			$success = update_option('se_catSubscribers', $se_catSubscribers);
			if($success === false){
				header("HTTP/1.0 409 Could not update user settings (meta) !");
				exit;				
			}else{
				echo json_encode( array('success' => true, 'catID' => $catID, 'userID' => $userID) );
			}
			exit;
		}
		
		/**
		 * Removes user subscriptions via AJAX
		 */
		function deleteEmailCatAJAX(){
			$nonce = $_POST['nonce'];
			if( !wp_verify_nonce($nonce, 'se_nonce') ){
				header("HTTP/1.0 409 Security Check.");
				exit;
			}
			
			if( empty($_POST['userID']) ){
				header("HTTP/1.0 409 Could not find userID!");
				exit;
			}
			
			if( empty($_POST['catID']) ){
				header("HTTP/1.0 409 Could not find catID!");
				exit;				
			}

			global $current_user;
			$userID = (int) $_POST['userID'];

			if($userID != $current_user->ID){
				header("HTTP/1.0 409 User IDs do not match!");
				exit;
			}

			$catID  = (int) $_POST['catID'];
			
			$se_catSubscribers = get_option('se_catSubscribers');
			$user = get_userdata($userID);		
			
			if( empty($se_catSubscribers[$catID][$userID]) ){
				header("HTTP/1.0 409 Could not find your subscription!");
				exit;
			}else{
				unset( $se_catSubscribers[$catID][$userID] );
				update_option('se_catSubscribers', $se_catSubscribers);
				echo json_encode( array('success' => true, 'catID' => $catID, 'userID' => $userID) );				
			}
			exit;
		}
		
	}//end se_optionsWidgetAJAX class
}
?>