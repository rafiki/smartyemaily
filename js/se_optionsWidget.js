/**
 * JS handles front-end click events
 * for SmartyEmaily's se_optionsWidget.php widget.
 *
 * @see se_optionsWidgetAJAX.php for AJAX handling
 */
 
 var se_optionsWidget = {
 	/**
 	 * Saves a cat subscription
 	 */
 	saveEmailCat: function(userID, catID, widgetID){
			var thisObj = this;
			$.ajax({
				url				   : seJS.ajaxurl,
				type      : 'POST',
				data			   : {action: 'saveEmailCatAJAX', nonce: seJS.seNonce, userID: userID, catID: catID},
				dataType  : 'json',
				success  : function(response, statusText, jqXHR){
					var subscriptionList = $('#' + widgetID + '-subscriptions');
	 			var catName          = $('#' + widgetID + '-dropdown option:selected').text();
 	  	var deleteImg        = '<img src="' + seJS.IMAGE_PATH + '/no.png' + '" id="' + widgetID + '-delete" class="se_removeSubscription" data-catid="' + catID + '" data-userid="' + userID + '" />';
 	  	var subscriptionLbl  = $('<p class="se_subscription">' + catName + deleteImg + '</p>');
	 			subscriptionList.append(subscriptionLbl);
	 			thisObj.enableDeleteHover(subscriptionLbl);
	 			thisObj.removeSubButton($(subscriptionLbl.find('img')));
				},
				error    : function(jqXHR, statusText, errorThrown){
						alert(errorThrown);
				}
			})
 	},
 	/**
 	 * Handles adding a category subscription
 	 * on the front end.
 	 */
 	addEmailCatButton: function(button){
 		var thisObj = this;
 		var widgetID = button.attr('data-widgetid');
 		var dropDown = $('#' + widgetID + '-dropdown');
 		button.click(function(){
 			var catID  = dropDown.val();
 			var userID = $(this).attr('data-userid');
				thisObj.saveEmailCat(userID, catID, widgetID);
 		});
 	},
 	/**
 	 * Sends AJAX call to server to delete category
 	 * subscription.
 	 */
 	removeCatSubscription: function(catID, userID, subscriptionLbl){
			var thisObj = this;
			$.ajax({
				url				   : seJS.ajaxurl,
				type      : 'POST',
				data			   : {action: 'deleteEmailCatAJAX', nonce: seJS.seNonce, userID: userID, catID: catID},
				dataType  : 'json',
				success  : function(response, statusText, jqXHR){
						subscriptionLbl.remove();
				},
				error    : function(jqXHR, statusText, errorThrown){
						alert(errorThrown);
				}
			}) 	
 	},
 	/**
 	 * Handles delete category subscriptions on the front end
 	 */
 	removeSubButton: function(removeButton){
 		var thisObj  = this;
 		removeButton.click(function(){
 			var catID  = $(this).attr('data-catid');
 			var userID = $(this).attr('data-userid');
 			thisObj.removeCatSubscription(catID, userID, $(this).parent());
 		});
 	},
 	/**
 	 * Binds the saveCommentButton function to button's
 	 * click event.
 	 * @param HTMLButton Element button A
 	 */
 	saveCommentButtonClick: function(button){
 		var thisObj = this;
 		button.click(function(){
 			var userID = $(this).attr('data-userid');
 			thisObj.saveSettings(userID);
 		})
 	},
 	/**
 	 * Shows the delete button for the DOMelement that (in this case <p>)
 	 * that contains the label for the category subscription
 	 */
 	enableDeleteHover: function(subscriptionElem){
			subscriptionElem.hover(
			 			function(){ $(this).find('img').show() }, 
			 			function(){ $(this).find('img').hide() }); 	
 	},
 	/**
 	 * Calls any JS init methods necessary for when
 	 * the page loads.
 	 */
 	init: function(){
 		this.addEmailCatButton($('.se_addEmailCats'), $('#se_emailCats'));
 		this.removeSubButton($('.se_removeSubscription'));
 		this.enableDeleteHover($('.se_subscription'));
 	}
 }
 
 if($ == undefined){
 	var $ = jQuery;
 }
 
 $(document).ready(function(){
		se_optionsWidget.init();
	});