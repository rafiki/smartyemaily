<?php
/*
Plugin Name: SmartyEmaily
Plugin URI: http://www.rafilabs.com/smartyemaily.php
Description: Provides the current logged in user the option to subcribe to a variety of email notifications.
Version: 0.1
Author: Rafi Y
Author URI: http://www.rafilabs.com 
*/

if($_SERVER['SERVER_NAME']=='localhost'){;
	require_once($_SERVER['DOCUMENT_ROOT'].'/iel-dev/wp-includes/pluggable.php');
}else{
	require_once ($_SERVER['DOCUMENT_ROOT'].'/wp-includes/pluggable.php');
}

//define("IMAGE_PATH", plugins_url('/images', __FILE__));
define("PLUGIN_PATH", plugins_url('/', __FILE__));

if ( !class_exists("smartyemaily") ) {
	/**
	 * Base class for SmartyEmaily. Used to initialize
	 * any necessary JS/CSS scripts + any other initialization
	 * scripts.
	 */
	class smartyemaily{
		function __construct(){
			$this->se_init();
			require_once( 'se_optionsWidget.php' );
			if(class_exists('se_optionsWidget')){
				se_optionsWidget::init();
			}			
		}
		
		function se_init(){
			self::enqueueJS();
		}
		
		static function enqueueJS(){
			wp_enqueue_script('jquery');
		}
		
		/**
		 * Remove se_emailCats table and emailComments user_meta
		 */
		function uninstallSE(){
			delete_option('se_post_subscribers');
		}
		
		function send_post_notifications($post){
			global $wp_query;
			$se_catSubscribers = get_option('se_catSubscribers');
			$categories = get_the_category($post->ID);
			
			if( !empty($categories) && !empty($se_catSubscribers)){
				$emails = array();
				//Go through each category and accumulate the necessary e-mail addresses
				foreach($categories as $category){
					$catID = $category->term_id;
					if( !empty($se_catSubscribers[$catID]) ){
						foreach($se_catSubscribers[$catID] as $userID => $trash){
							$user      = get_userdata($userID);
							$userEmail = array( $userID => $user->user_email );
							if( !in_array($userEmail, $emails) ){
								$emails   = $emails + $userEmail;
							}
						}
					}
				}
			}
			
			//Setup wp_mail
			add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));
			
			$to      = get_bloginfo('admin_email');
			$subject = '[' . get_bloginfo('name') . '] ' . $post->post_title;
			
			if(!$wp_query->is_single){ 
				$wp_query->is_single = true; //
			}
			
			$html .= '<p> A new post has been posted in ' . get_bloginfo('name') . ': <a href="' . get_permalink($post->ID) . '">' . $post->post_title .'</a></p>';
			$html .= '<p>' . get_the_post_thumbnail($post->ID, 'thumbnail') . do_shortcode($post->post_content) . '</p>';
			$headers[] = 'Bcc: ' . implode(", ", $emails);
			
			wp_mail($to, $subject, $html, $headers);
			
			return $post;
		}
		
	}//end smartyemaily class
				
	if(class_exists('smartyemaily')){
		$new_se = new smartyemaily();
	}
	
	if(isset($new_se)){
		register_uninstall_hook( __FILE__, array( 'smartyemaily', 'uninstallSE' ) );
		add_action('draft_to_publish', array( 'smartyemaily', 'send_post_notifications') );
		add_action('widgets_init', create_function('', 'return register_widget("se_optionsWidget");'));
	}
	
}